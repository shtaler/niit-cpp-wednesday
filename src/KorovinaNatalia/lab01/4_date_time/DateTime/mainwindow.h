#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <time.h>
#include <date_time.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void update1();
    void update2();
    bool check(int sec, int min, int hour, int day, int month, int year);
    ~MainWindow();

private slots:
    void on_pbSToday1_clicked();
    void on_pbSToday2_clicked();
    void on_pbDiff_clicked();

    void on_leYear1_editingFinished();
    void on_leMonth1_editingFinished();
    void on_leDay1_editingFinished();
    void on_leHour1_editingFinished();
    void on_leMinute1_editingFinished();
    void on_leSecond1_editingFinished();

    void on_leYear2_editingFinished();
    void on_leMonth2_editingFinished();
    void on_leDay2_editingFinished();
    void on_leHour2_editingFinished();
    void on_leMinute2_editingFinished();
    void on_leSecond2_editingFinished();

    void on_pbBack1_clicked();
    void on_pbForward1_clicked();

    void on_pbBack2_clicked();
    void on_pbForward2_clicked();

private:
    Ui::MainWindow *ui;
    date_time *tm1;
    date_time *tm2;


};

#endif // MAINWINDOW_H
