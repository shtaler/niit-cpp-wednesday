#include "automata.h"
#include <QString>

Automata::Automata()
{
    state=OFF;
    cash=0;
    change=0;
}

void Automata::on() // set automata on
{
    state=WAIT;
}

void Automata::off() //set automata off
{
    state=OFF;
}

void Automata::coin(int addition) // deposit money
{
    if (state==WAIT ||state==CHECK)
        state=ACCEPT;
    if(state==ACCEPT)
        cash+=addition;
}

QString Automata::printState() const //print current status
{
    QString status;
    switch (state)
    {
    case OFF:
    {
        status="OFF";
    }

    case WAIT:
    {
        status="Choose the drink and deposit money";
        break;
    }

    case ACCEPT:
    {
        status="Current deposit is: " + (QString::number(cash)) +" RUB" ;
        break;
    }
    case CHECK:

    {
        status="You need more money to choose this drink. "
               "Please add money, choose another drink or cancel serving. "
               "Current deposit is: " + (QString::number(cash)) + " RUB";
        break;
    }
    case COOK:
    {
        status="Your drink is being cooked now. Please wait";
        break;
    }

    case READY:
    {
        status="Your drink is cooked. Enjoy drinking!";
        break;
    }
    default:
    {
        status="Error!";
        break;
    }
    }
    return status;
}

void Automata::choice() // drink choosing
{
    state = CHECK;
}

bool Automata::check(int price) // check for the required money
{
    if (price<=cash)
    {
        change=cash-price;
        return true;
    }
    else
        return false;
}

int Automata::getChange () const
{
    return change;
}

void Automata::cancel() // cancel the order
{
    state=WAIT;
    change=cash;
}

void Automata::cook() //simulation of cooking process
{
    state=COOK;
}

void Automata::ready()
{
    state=READY;
}

void Automata::finish() // finish serving
{
    state=WAIT;
    change=0;
    cash=0;
}
