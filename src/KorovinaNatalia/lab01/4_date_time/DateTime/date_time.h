#ifndef DATE_TIME_H
#define DATE_TIME_H
#include <time.h>
#include <string>


class date_time
{
private:
    time_t now;

public:
    date_time();
    date_time(int sec, int min, int hour, int day, int month, int year);
    time_t getTodaySec() const; //получить now время в формате секунд
    struct tm *getToday(); // получить now время в виде структуры
    std::string printToday()const; //вывод даты now в формате строки
    std::string printYesterday()const; //вывод даты вчера в формате строки
    std::string printTomorrow()const; //вывод даты завтра в формате строки

    std::string printMonth()const; //вывод месяца в формате строки
    std::string printWeekDay()const; // вывод дня недели в формате строки
    struct tm *printFuture(int offset)const; //вывод даты через offset дней в будущем
    struct tm *printPast(int offset)const; // вывод даты через offset дней в прошлом
    int calcDifference(time_t tm2)const; //вывод разницы между датами (кол-во дней)
};

#endif // DATE_TIME_H
