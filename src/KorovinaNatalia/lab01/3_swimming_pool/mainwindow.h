#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <circle.h>

float const CostOfFence=2000.0;
float const CostOfTrack=1000.0;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void Calculate();
    ~MainWindow();

private slots:
    void on_pbPlus_clicked();

private:
    Ui::MainWindow *ui;
    circle *sPool;
    circle *track;
    circle *fence;
};




#endif // MAINWINDOW_H
