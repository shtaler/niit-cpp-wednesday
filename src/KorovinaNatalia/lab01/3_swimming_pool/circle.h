#ifndef CIRCLE_H
#define CIRCLE_H

class circle
{
private:
    double radius;
    double ference;
    double area;
public:
   // circle(): radius(6378.1)
   // {}
    void setRadius(double r);
    void setFerence(double f);
    void setArea(double a);
    double getRadius();
    double getFerence();
    double getArea();
};


#endif // CIRCLE_H
