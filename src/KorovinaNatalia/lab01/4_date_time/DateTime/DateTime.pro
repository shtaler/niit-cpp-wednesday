#-------------------------------------------------
#
# Project created by QtCreator 2016-04-26T20:51:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DateTime
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    date_time.cpp

HEADERS  += mainwindow.h \
    date_time.h

FORMS    += mainwindow.ui
