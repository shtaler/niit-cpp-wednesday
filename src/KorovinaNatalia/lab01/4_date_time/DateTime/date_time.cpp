#include "date_time.h"
#include <string>
#define SIZE 100

date_time::date_time( )
{
    now=time(NULL);
}

date_time::date_time(int sec, int min, int hour, int day, int month, int year)
{
    struct tm t;
    t.tm_sec=sec;
    t.tm_min=min;
    t.tm_hour=hour;
    t.tm_mday=day;
    t.tm_mon=month-1;
    t.tm_year=year-1900; //1900!!!!
    now=mktime(&t);
}

time_t date_time::getTodaySec() const
{

    return now;
}

struct tm* date_time::getToday()
{
    return localtime(&now);
}

std::string date_time::printToday () const
{
    char output[SIZE]; // ex: Monday 11 April 2016
    struct tm *t = localtime(&now);
    strftime(output, SIZE,"%A, %d %B %Y",t);
    return output;
}

std::string date_time::printYesterday () const
{
    char output[SIZE]; // ex: Monday 11 April 2016
    time_t TimeYest=now - 24*3600;
    struct tm *t = localtime(&TimeYest);
    strftime(output, SIZE,"%A, %d %B %Y",t);
    return output;
}

std::string date_time::printTomorrow () const
{
    char output[SIZE]; // ex: Monday 11 April 2016
    time_t TimeTom=now + 24*3600;
    struct tm *t = localtime(&TimeTom);
    strftime(output, SIZE,"%A, %d %B %Y",t);
    return output;
}

std::string date_time::printMonth () const
{
    char output[SIZE]; // April
    struct tm *t = localtime(&now);
    strftime(output, SIZE,"%B",t);
    return output;
}

std::string date_time::printWeekDay () const
{
    char output[SIZE]; // Monday
    struct tm *t = localtime(&now);
    strftime(output, SIZE,"%A",t);
    return output;
}

int date_time::calcDifference(time_t tm2) const
{

    double diffSec=difftime(now,tm2);
    if (diffSec>=0)
        return diffSec/(24*60*60);
    else
        return -diffSec/(24*60*60);
}

struct tm * date_time::printFuture(int offset) const
{
    time_t OffTime = now + (offset*24*60*60);
    return localtime(&OffTime);
}

struct tm * date_time::printPast(int offset) const
{
    time_t OffTime = now - (offset*24*60*60);
    return localtime(&OffTime);
}
