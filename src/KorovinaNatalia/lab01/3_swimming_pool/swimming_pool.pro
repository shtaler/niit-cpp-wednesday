#-------------------------------------------------
#
# Project created by QtCreator 2016-03-23T19:00:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = swimming_pool
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    circle.cpp

HEADERS  += mainwindow.h \
    circle.h

FORMS    += mainwindow.ui
