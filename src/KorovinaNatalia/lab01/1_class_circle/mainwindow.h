#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <circle.h>

enum CurrentEditingType {ERadius, EFerence, EArea} ;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbCalculate_clicked();

    void on_leFerence_editingFinished();

    void on_leRadius_editingFinished();

    void on_leArea_editingFinished();

private:
    Ui::MainWindow *ui;
    circle *circle1;
    CurrentEditingType editingType ;

};

#endif // MAINWINDOW_H
