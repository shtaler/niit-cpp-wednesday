/*/////////EARTH AND ROPE/////////*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    earth= new circle;
    rope=new circle;
    earth->setRadius(RE);
    rope->setRadius(RE);
    ui->leRadius->setText(QString::number(earth->getRadius()));
    ui->leFerence->setText(QString::number(earth->getFerence()));
    ui->leLenght->setText(QString::number(rope->getFerence()));
    ui->leGap->setText(QString::number((rope->getRadius())-(earth->getRadius())));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbPlus_clicked()
{
    rope->setFerence((rope->getFerence())+1);
    ui->leLenght->setText(QString::number(rope->getFerence()));
    ui->leGap->setText(QString::number((rope->getRadius())-(earth->getRadius())));
}
