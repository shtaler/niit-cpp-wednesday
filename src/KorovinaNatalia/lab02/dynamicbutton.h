#ifndef DYNAMICBUTTON_H
#define DYNAMICBUTTON_H
#include <QPushButton>

class DynamicButton : public QPushButton
{
        Q_OBJECT
public:
    explicit DynamicButton(QWidget *parent = 0, int number=0, int cost=0, QString name=0);
    //explicit DynamicButton(QWidget *parent = 0);
    int getNumber() const;
    QString getName() const;
    int getCost() const;

    void setNumber(int number);
    void setName(QString name);
    void setCost(int cost);
 private:
     int DrinkNumber;
     int DrinkCost;
     QString DrinkName;
};

#endif // DYNAMICBUTTON_H
