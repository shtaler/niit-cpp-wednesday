#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <automata.h>
#include <QTimer>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QProgressBar>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

private:
    void xmlParser(); // Upload Menu from file and create buttons
    void returnChange();
    Ui::MainWindow *ui;
    Automata *automata;
    QTimer *qTimer;
    QPushButton *buttonChange;
    QLineEdit *LeChange;
    QLabel *labelChange;
    QProgressBar *progressBar;

private slots:

    void slotAddCash(int cash);
    void slotDrinkChosen();
    void slotServing();
    void slotTakeChange();

    void on_pbCash1_clicked();
    void on_pbCash2_clicked();
    void on_pbCash5_clicked();
    void on_pbCash10_clicked();
    void on_pbCash10b_clicked();
    void on_pbCash50_clicked();
    void on_pbCash100_clicked();
    void on_pbCash500_clicked();
    void on_pbCancel_clicked();

signals:
    void signalAddCash(int cash);
};

#endif // MAINWINDOW_H
