#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "date_time.h"
#include "time.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->leYear1->setText("1900");
    ui->leMonth1->setText("1");
    ui->leDay1->setText("1");
    ui->leHour1->setText("0");
    ui->leMinute1->setText("0");
    ui->leSecond1->setText("0");
    ui->leJump1->setText("0");

    ui->leYear2->setText("1900");
    ui->leMonth2->setText("1");
    ui->leDay2->setText("1");
    ui->leHour2->setText("0");
    ui->leMinute2->setText("0");
    ui->leSecond2->setText("0");
    ui->leJump2->setText("0");


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbSToday1_clicked()
{
    tm1 = new date_time;

    ui->leYear1->setText(QString::number(tm1->getToday()->tm_year+1900));
    ui->leMonth1->setText(QString::number(tm1->getToday()->tm_mon+1));
    ui->leDay1->setText(QString::number(tm1->getToday()->tm_mday));
    ui->leHour1->setText(QString::number(tm1->getToday()->tm_hour));
    ui->leMinute1->setText(QString::number(tm1->getToday()->tm_min));
    ui->leSecond1->setText(QString::number(tm1->getToday()->tm_sec));
    ui->leToday1->setText(QString::fromStdString(tm1->printToday()));
    ui->leTomorrow1->setText(QString::fromStdString(tm1->printTomorrow()));
    ui->leYesterday1->setText(QString::fromStdString(tm1->printYesterday()));

}

void MainWindow::on_pbSToday2_clicked()
{
    tm2 = new date_time;

    ui->leYear2->setText(QString::number(tm2->getToday()->tm_year+1900));
    ui->leMonth2->setText(QString::number(tm2->getToday()->tm_mon+1));
    ui->leDay2->setText(QString::number(tm2->getToday()->tm_mday));
    ui->leHour2->setText(QString::number(tm2->getToday()->tm_hour));
    ui->leMinute2->setText(QString::number(tm2->getToday()->tm_min));
    ui->leSecond2->setText(QString::number(tm2->getToday()->tm_sec));
    ui->leToday2->setText(QString::fromStdString(tm2->printToday()));
    ui->leTomorrow2->setText(QString::fromStdString(tm2->printTomorrow()));
    ui->leYesterday2->setText(QString::fromStdString(tm2->printYesterday()));

}

void MainWindow::on_pbDiff_clicked()
{
    tm1 = new date_time(ui->leSecond1->text().toInt(),ui->leMinute1->text().toInt(),ui->leHour1->text().toInt(),
                        ui->leDay1->text().toInt(),ui->leMonth1->text().toInt(),ui->leYear1->text().toInt());
    tm2 = new date_time(ui->leSecond2->text().toInt(),ui->leMinute2->text().toInt(),ui->leHour2->text().toInt(),
                        ui->leDay2->text().toInt(),ui->leMonth2->text().toInt(),ui->leYear2->text().toInt());
    ui->leDiff->setText(QString::number(tm1->calcDifference(tm2->getTodaySec())));
}

void MainWindow::update1()
{
    if (!(check(ui->leSecond1->text().toInt(),ui->leMinute1->text().toInt(),ui->leHour1->text().toInt(),
                ui->leDay1->text().toInt(),ui->leMonth1->text().toInt(),ui->leYear1->text().toInt())))
    {
        tm1 = new date_time(ui->leSecond1->text().toInt(),ui->leMinute1->text().toInt(),ui->leHour1->text().toInt(),
                            ui->leDay1->text().toInt(),ui->leMonth1->text().toInt(),ui->leYear1->text().toInt());
        ui->leToday1->setText(QString::fromStdString(tm1->printToday()));
        ui->leTomorrow1->setText(QString::fromStdString(tm1->printTomorrow()));
        ui->leYesterday1->setText(QString::fromStdString(tm1->printYesterday()));

    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Incorrect inputed value(s)");
        msgBox.exec();
    }

}

void MainWindow::update2()
{
    if (!(check(ui->leSecond2->text().toInt(),ui->leMinute2->text().toInt(),ui->leHour2->text().toInt(),
                ui->leDay2->text().toInt(),ui->leMonth2->text().toInt(),ui->leYear2->text().toInt())))
    {
        tm2 = new date_time(ui->leSecond2->text().toInt(),ui->leMinute2->text().toInt(),ui->leHour2->text().toInt(),
                            ui->leDay2->text().toInt(),ui->leMonth2->text().toInt(),ui->leYear2->text().toInt());
        ui->leToday2->setText(QString::fromStdString(tm2->printToday()));
        ui->leTomorrow2->setText(QString::fromStdString(tm2->printTomorrow()));
        ui->leYesterday2->setText(QString::fromStdString(tm2->printYesterday()));
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Incorrect inputed value(s)");
        msgBox.exec();
    }
}

bool MainWindow::check(int sec, int min, int hour, int day, int month, int year)
{
    if ((sec>=0 && sec<=60) && (min>=0 && min<=60) && (hour>=0 && hour<24) &&
            (day>0 && day<=31) && (month>0 && month<=12) && (year>0))
        return 0;
    else
        return 1;
}

void MainWindow::on_leYear1_editingFinished()
{
    update1();
}

void MainWindow::on_leMonth1_editingFinished()
{
    update1();
}

void MainWindow::on_leDay1_editingFinished()
{
    update1();
}

void MainWindow::on_leHour1_editingFinished()
{
    update1();
}

void MainWindow::on_leMinute1_editingFinished()
{
    update1();
}

void MainWindow::on_leSecond1_editingFinished()
{
    update1();
}

void MainWindow::on_leYear2_editingFinished()
{
    update2();
}

void MainWindow::on_leMonth2_editingFinished()
{
    update2();
}

void MainWindow::on_leDay2_editingFinished()
{
    update2();
}

void MainWindow::on_leHour2_editingFinished()
{
    update2();
}

void MainWindow::on_leMinute2_editingFinished()
{
    update2();
}

void MainWindow::on_leSecond2_editingFinished()
{
    update2();
}

void MainWindow::on_pbBack1_clicked()
{
    tm1 = new date_time(ui->leSecond1->text().toInt(),ui->leMinute1->text().toInt(),ui->leHour1->text().toInt(),
                        ui->leDay1->text().toInt(),ui->leMonth1->text().toInt(),ui->leYear1->text().toInt());;
    int offset=ui->leJump1->text().toInt();
    tm1->printPast(offset);
    ui->leYear1->setText(QString::number(tm1->printPast(offset)->tm_year+1900));
    ui->leMonth1->setText(QString::number(tm1->printPast(offset)->tm_mon+1));
    ui->leDay1->setText(QString::number(tm1->printPast(offset)->tm_mday));

    update1();
}

void MainWindow::on_pbForward1_clicked()
{
    tm1 = new date_time(ui->leSecond1->text().toInt(),ui->leMinute1->text().toInt(),ui->leHour1->text().toInt(),
                        ui->leDay1->text().toInt(),ui->leMonth1->text().toInt(),ui->leYear1->text().toInt());;
    int offset=ui->leJump1->text().toInt();
    tm1->printFuture(offset);
    ui->leYear1->setText(QString::number(tm1->printFuture(offset)->tm_year+1900));
    ui->leMonth1->setText(QString::number(tm1->printFuture(offset)->tm_mon+1));
    ui->leDay1->setText(QString::number(tm1->printFuture(offset)->tm_mday));

    update1();

}

void MainWindow::on_pbBack2_clicked()
{
    tm2 = new date_time(ui->leSecond2->text().toInt(),ui->leMinute2->text().toInt(),ui->leHour2->text().toInt(),
                        ui->leDay2->text().toInt(),ui->leMonth2->text().toInt(),ui->leYear2->text().toInt());;
    int offset=ui->leJump2->text().toInt();
    tm2->printPast(offset);
    ui->leYear2->setText(QString::number(tm2->printPast(offset)->tm_year+1900));
    ui->leMonth2->setText(QString::number(tm2->printPast(offset)->tm_mon+1));
    ui->leDay2->setText(QString::number(tm2->printPast(offset)->tm_mday));

    update2();
}

void MainWindow::on_pbForward2_clicked()
{
    tm2 = new date_time(ui->leSecond2->text().toInt(),ui->leMinute2->text().toInt(),ui->leHour2->text().toInt(),
                        ui->leDay2->text().toInt(),ui->leMonth2->text().toInt(),ui->leYear2->text().toInt());;
    int offset=ui->leJump2->text().toInt();
    tm2->printFuture(offset);
    ui->leYear2->setText(QString::number(tm2->printFuture(offset)->tm_year+1900));
    ui->leMonth2->setText(QString::number(tm2->printFuture(offset)->tm_mon+1));
    ui->leDay2->setText(QString::number(tm2->printFuture(offset)->tm_mday));

    update2();
}
