#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->leRadius->setText("0");
    ui->leArea->setText("0");
    ui->leFerence->setText("0");
    circle1= new circle;
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pbCalculate_clicked()
{
    switch (editingType)
    {
    case ERadius:
    {
        circle1->setRadius(ui->leRadius->text().toDouble());
        ui->leFerence->setText(QString::number(circle1->getFerence()));
        ui->leArea->setText(QString::number(circle1->getArea()));
        break;
    }
    case EFerence:
    {
        circle1->setFerence(ui->leFerence->text().toDouble());
        ui->leRadius->setText(QString::number(circle1->getRadius()));
        ui->leArea->setText(QString::number(circle1->getArea()));
        break;
    }
    case EArea:
    {
        circle1->setArea(ui->leArea->text().toDouble());
        ui->leRadius->setText(QString::number(circle1->getRadius()));
        ui->leFerence->setText(QString::number(circle1->getFerence()));
        break;
    }

    }

    
}

void MainWindow::on_leRadius_editingFinished()
{
    editingType=ERadius;
}

void MainWindow::on_leFerence_editingFinished()
{
    editingType=EFerence;
}


void MainWindow::on_leArea_editingFinished()
{
    editingType=EArea;
}
