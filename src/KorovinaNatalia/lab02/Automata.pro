#-------------------------------------------------
#
# Project created by QtCreator 2016-05-02T23:13:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Automata
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dynamicbutton.cpp \
    automata.cpp

HEADERS  += mainwindow.h \
    dynamicbutton.h \
    automata.h

FORMS    += mainwindow.ui

INCLUDEPATH += .
