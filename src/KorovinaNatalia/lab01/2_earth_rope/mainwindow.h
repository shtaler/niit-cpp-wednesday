#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <circle.h>

double const RE= 6378.1;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbPlus_clicked();

private:
    Ui::MainWindow *ui;
    circle *earth;
    circle *rope;
};

#endif // MAINWINDOW_H
