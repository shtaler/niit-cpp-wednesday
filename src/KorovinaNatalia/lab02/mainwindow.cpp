#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dynamicbutton.h"
#include "qfile.h"
#include "qmessagebox.h"
#include <QXmlStreamReader>
#include <automata.h>
#include <QTimer>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QProgressBar>

void MainWindow::xmlParser() //Upload Menu from file and create buttons
{
    QString filename;

    filename="/home/shtaler/DrinksList.xml";
    QFile * file=new QFile(filename);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox mess;
        mess.setText("File error!");
        mess.exec();
        return;
    }
    QXmlStreamReader xml(file);
    QXmlStreamReader::TokenType token;
    while(!xml.atEnd() && !xml.hasError())
    {
        token=xml.readNext();
        if (token== QXmlStreamReader::StartDocument)
            continue;
        if (token == QXmlStreamReader::StartElement)

        {
            QString name;
            int id, cost;
            if (xml.name()=="drinks")
                continue;
            if (xml.name()=="drink")
            {

                QXmlStreamAttributes attrs=xml.attributes();
                if (attrs.hasAttribute("id"))
                    id=attrs.value("id").toInt();
                if (attrs.hasAttribute("name"))
                    name=attrs.value("name").toString();
                if (attrs.hasAttribute("cost"))
                    cost=attrs.value("cost").toInt();
                DynamicButton *button = new DynamicButton(this, id, cost, name);
                button->setText(button->getName() + "\n"+ QString::number(button->getCost())+" RUB");
                ui->verticalLayout->addWidget(button);
                connect(button, SIGNAL(clicked()), this, SLOT(slotDrinkChosen()));


            }

        }

    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->teStatus->setReadOnly(true);

    xmlParser();

    automata = new Automata;
    qTimer=new QTimer;

    automata->on();
    ui->teStatus->setText(automata->printState());

    connect(this, &MainWindow::signalAddCash, this, &MainWindow::slotAddCash);
    connect(this->qTimer, SIGNAL(timeout()), this, SLOT(slotServing()));
}

void MainWindow::returnChange()
{
    labelChange = new QLabel;
    labelChange->setText("Your change is here:");
    ui->verticalLayout_3->addWidget(labelChange);

    LeChange = new QLineEdit;
    LeChange->setText(QString::number(automata->getChange()));
    LeChange->setReadOnly(true);
    ui->verticalLayout_3->addWidget(LeChange);

    buttonChange = new QPushButton;
    buttonChange->setText("Take change!");
    connect(buttonChange, SIGNAL(clicked()), this, SLOT(slotTakeChange()));
    ui->verticalLayout_3->addWidget(buttonChange);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbCash1_clicked()
{
    emit signalAddCash(ui->pbCash1->text().section(" ", 0, 0).toInt());
}

void MainWindow::on_pbCash2_clicked()
{
    emit signalAddCash(ui->pbCash2->text().section(" ", 0, 0).toInt());
}

void MainWindow::on_pbCash10b_clicked()
{
    emit signalAddCash(ui->pbCash10b->text().section(" ", 0, 0).toInt());;
}

void MainWindow::on_pbCash5_clicked()
{
    emit signalAddCash(ui->pbCash5->text().section(" ", 0, 0).toInt());
}

void MainWindow::on_pbCash10_clicked()
{
    emit signalAddCash(ui->pbCash10->text().section(" ", 0, 0).toInt());
}

void MainWindow::on_pbCash50_clicked()
{
    emit signalAddCash(ui->pbCash50->text().section(" ", 0, 0).toInt());;
}

void MainWindow::on_pbCash100_clicked()
{
    emit signalAddCash(ui->pbCash100->text().section(" ", 0, 0).toInt());
}

void MainWindow::on_pbCash500_clicked()
{
    emit signalAddCash(ui->pbCash500->text().section(" ", 0, 0).toInt());
}

void MainWindow::slotAddCash(int cash)
{
    automata->coin(cash);
    ui->teStatus->setText(automata->printState());
}

void MainWindow::slotDrinkChosen()
{
    DynamicButton *button = (DynamicButton*) sender();
    automata->choice();
    if (automata->check(button->getCost()))
    {
        automata->cook();
        ui->teStatus->setText(automata->printState());
        ui->groupBox->setEnabled(false);
        ui->groupBox_2->setEnabled(false);
        ui->groupBox_3->setEnabled(false);
        ui->pbCancel->setEnabled(false);
        progressBar = new QProgressBar;
        progressBar->setValue(0);
        ui->verticalLayout_2->addWidget(progressBar);

        qTimer->start(50);

    }
    else
    {
        ui->teStatus->setText(automata->printState());
    }

}

void MainWindow::slotServing()
{
    if (progressBar->value()<100)
    {
        progressBar->setValue((progressBar->value())+1);
    }
    else
    {
        qTimer->stop();
        automata->ready();
        ui->teStatus->setText(automata->printState());

        if(automata->getChange()>0)
        {
            delete progressBar;
            returnChange();
        }

        else
        {
            ui->groupBox->setEnabled(true);
            ui->groupBox_2->setEnabled(true);
            ui->groupBox_3->setEnabled(true);
            ui->pbCancel->setEnabled(true);
            automata->finish();
            automata->printState();
            progressBar->setValue(0);
        }
    }
}

void MainWindow::slotTakeChange()
{
    ui->groupBox->setEnabled(true);
    ui->groupBox_2->setEnabled(true);
    ui->groupBox_3->setEnabled(true);
    ui->pbCancel->setEnabled(true);
    automata->finish();
    ui->teStatus->setText(automata->printState());
    delete buttonChange;
    delete LeChange;
    delete labelChange;
}

void MainWindow::on_pbCancel_clicked()
{
    automata->cancel();
    if(automata->getChange()>0)
    {
        ui->groupBox->setEnabled(false);
        ui->groupBox_2->setEnabled(false);
        ui->groupBox_3->setEnabled(false);
        ui->pbCancel->setEnabled(false);
        returnChange();
    }
}
