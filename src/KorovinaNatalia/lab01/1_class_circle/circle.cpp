#include "circle.h"
#include <math.h>

void circle::setRadius(double r)
{
    radius=r;
    ference=2*M_PI*r;
    area=M_PI*r*r;
}
void circle::setFerence(double f)
{
    ference=f;
    radius=ference/(2*M_PI);
    area=ference/2*radius;
}
void circle::setArea(double a)
{
    area=a;
    radius=sqrt(area/M_PI);
    ference=2*M_PI*radius;
}
double circle::getRadius()
    {return radius;}
double circle::getFerence()
    {return ference;}
double circle::getArea()
    {return area;}
