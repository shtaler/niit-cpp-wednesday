#include "mainwindow.h"
#include "ui_mainwindow.h"

void calculate ();

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    sPool= new circle;
    track=new circle;
    fence=new circle;

    ui->leRadius->setReadOnly(true);
    ui->leRadius->setReadOnly(true);
    ui->leAreaTrack->setReadOnly(true);
    ui->leLenghtFence->setReadOnly(true);
    ui->leCostFence->setReadOnly(true);
    ui->leCostTrack->setReadOnly(true);

    sPool->setRadius(3);
    Calculate();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pbPlus_clicked()
{
    sPool->setRadius((sPool->getRadius())+1);
    Calculate();
}

void MainWindow::Calculate()
{

    track->setRadius(sPool->getRadius()+1);
    fence->setRadius((track->getRadius()));
    ui->leRadius->setText(QString::number(sPool->getRadius()));
    ui->leAreaTrack->setText(QString::number(track->getArea()));
    ui->leLenghtFence->setText(QString::number(fence->getFerence()));
    ui->leCostFence->setText(QString::number(CostOfFence*fence->getFerence()));
    ui->leCostTrack->setText(QString::number(CostOfTrack*(track->getArea()-(sPool->getArea()))));
    ui->leTotal->setText(QString::number((CostOfFence*(fence->getFerence()))+(CostOfTrack*(track->getArea()-(sPool->getArea())))));
}
