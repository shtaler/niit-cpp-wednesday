#ifndef AUTOMATA_H
#define AUTOMATA_H
#include <QString>

enum states {OFF, WAIT, ACCEPT,CHECK,COOK,READY};

class Automata
{
private:
 int cash;
 int change;
 states state;

public:
    Automata();
    void on(); // set automata on
    void off(); //set automata off
    void coin(int addition); // deposit money
    QString printState() const; //print current status
    void choice(); // drink choosing
    bool check(int price); // check for the required money
    int getChange () const; //calculate change
    void cancel(); // cancel the order
    void cook(); //simulation of cooking process
    void ready(); //drink is cooked
    void finish(); // finish serving
};

#endif // AUTOMATA_H
