#include "dynamicbutton.h"
#include <QPushButton>


DynamicButton::DynamicButton(QWidget *parent, int number, int cost, QString name ):
   QPushButton(parent)
{
    DrinkName=name;
    DrinkNumber=number;
    DrinkCost=cost;
    this->setObjectName("pb"+number);
}


 QString DynamicButton::getName () const
 {
     return DrinkName;
 }

int DynamicButton::getNumber() const
 {
     return DrinkNumber;
 }

int DynamicButton::getCost() const
 {
     return DrinkCost;
 }

void DynamicButton::setName(QString name)
{
    DrinkName=name;
}

void DynamicButton::setNumber(int number)
{
    DrinkNumber=number;
}

void DynamicButton::setCost(int cost)
{
    DrinkCost=cost;
}

